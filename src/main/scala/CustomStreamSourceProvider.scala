package org.apache.spark.sql.execution.streaming

import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.sources.{DataSourceRegister, StreamSourceProvider}
import org.apache.spark.sql.types.StructType

class CustomStreamSourceProvider extends StreamSourceProvider with DataSourceRegister {
  override def sourceSchema(sqlContext: SQLContext, schema: Option[StructType], providerName: String, parameters: Map[String, String]): (String, StructType) = ???

  override def createSource(sqlContext: SQLContext, metadataPath: String, schema: Option[StructType], providerName: String, parameters: Map[String, String]): Source = ???

  override def shortName(): String = ""
}
